<!-- $size: 16:9 -->
<!-- page_number: true -->
<!-- footer: Roland Stumpner GPLv3 -->


# Systemadministraton 2 Übung
* SAM 2 Übung
* vLAB-Storage-LVM


---
# Logical Volume Manager
* vLAB-Storage-LVM
* Version 20170327

---
# LVM Übung Übersicht
* LVM Voraussetzungen vLAB
* LVM Einführung
* LVM First Steps
* LVM Aufgaben
---
# LVM Brainstorming

---
# LVM Voraussetzungen vLAB
* VM ( Virtualbox / Vmware Workstation / Proxmox VM)
* 1 x CPU Core
* 2 GB RAM
* 2 x VM Disks mininum 2 GB
* Linux Ubuntu 16.04 LTS
---
# Logical Volume Manager Einführung
* Ist eine Abstraktionsschicht zum Verwalten von Speicherzuweisungen unabhängig von den Physikalischen Resourcen
* Physical Volume (pvdisplay / pvcreate)
* Volume Group (vgdisplay / vgcreate)
* Logical Volume (lvdisplay / lvcreate)
---
# Logical Volume Manager Einführung
![LVM Aufbau](lvm-aufbau.png)

---
# LVM Einführung Installation (Manual)
* Ubuntu 16.04 LTS Basis VM
---
# LVM with vLAB (Vagrant)
* git pull https://www.github.com/stumpi/vLab/sam-vlab-storage-lvm.git
---
## First Steps in Logical Volume Manager
* Add Disk to VM
* Check der Hardware Speicher
	```md
      lsblk
   ```
* Partitionierung der Harddisk
   ```md
      cfdisk /dev/sdx
    ```
* Erstellen der Physical Volume
   ```md
      pvcreate /dev/sdx
   ```
* Erstellen der Volume Group
	 ```md
		vgcreate data /dev/sdx1
	```
---
# First Steps with Logical Volume Manager
* Erstellen der Logical Volume
	```md
	lvcreate -L 2G -n backup data
	```
* Sichtung der Logical Volume
	```md
	lvdisplay
	```
* Erstellen eines Dateisystems
	```md
	mkfs.ext4 /dev/mapper/backup
	```
---
# LVM Aufgaben 1
* Textdatei mit Inhalt auf der Partition des Logical Volume erstellen
* Physical Volume erweitern ( Disk hinzufügen )
* Volume Group erweitern
* Logical Volume erweitern
* Dateisystem erweitern
* Lesen der Textdatei (ist noch der gleiche Inhalt vorhanden)
* Migration des Logical Volumes auf die neue Disk
* Auspflegen der alten Disk aus der Volume Group
---
# LVM Lösung
---
# LVM Cheat Sheet
* Physical Volume (pvdisplay / pvcreate)
* Volume Group (vgdisplay / vgcreate)
* Logical Volume (lvdisplay / lvcreate)
---
# Links
* http://teknixx.com/40-useful-linux-commands/
* http://gparted.org/livecd.php
---
# Notizen
![pvcreate](lvm-pvcreate.png)

---
![pvcreate](lvm-vgcreate.png)

---
![lvcreate](lvm-lvcreate.png)