# SAM vLab with the Logical Volume Manager (LVM)
This is a Gitlab Repository to do an easy Virtual Lab Environment with the Logical Volume Manger ( LVM ) Open Source Software.

#### Follow the Instructions of this Virtual Lab:

Option 1 (Online):
  * Browse https://gitpitch.com/rstumpner/sam-vlab-storage-lvm/master?grs=gitlab&t=simple

Option 2 (Local):
  * git clone https://gitlab.com/rstumpner/sam-vlab-storage-lvm
  * gem install showoff
  * showoff serve
  * Browse http://localhost:9090

#### Setup the Virtual Lab Environment

Requirements:
  * 2 GB Memory (minimal)
  * 1 x CPU Cores
  * Installation of Virtualbox (https://www.virtualbox.org/)

vLAB Environment:
  * Virtual Machine
    * 1 x vCPU
    * 2 GB RAM
    * OS Linux 64 Bit
    * 2 x Disk

Automatic Setup with Vagrant (https://www.vagrantup.com/) (local):
  * Download and Install Vagrant Package for your OS
    * On Linux
      * wget https://releases.hashicorp.com/vagrant/2.0.3/vagrant_2.0.3_x86_64.deb?_ga=2.257941326.439840422.1522128825-1814262215.1522128825
      * dpkg -i vagrant_2.0.3_x86_64.deb
    * On Windows
      * https://releases.hashicorp.com/vagrant/2.0.3/vagrant_2.0.3_x86_64.msi?_ga=2.89037278.439840422.1522128825-1814262215.1522128825
    * on macOS
      * https://releases.hashicorp.com/vagrant/2.0.3/vagrant_2.0.3_x86_64.dmg?_ga=2.257941326.439840422.1522128825-1814262215.1522128825

* Start the Setup the vLAB Environment with Vagrant
   ```md
      cd sam-vlab-storage-lvm/vlab/vagrant-virtualbox/
      vagrant up
    ```
 * Check the vLAB Setup
    ```md
    vagrant status
    ```
 * Login to work with a Node
   ```md
   vagrant ssh
   ```
#### Troubleshooting
