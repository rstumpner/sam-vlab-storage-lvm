---
marp: true

---
## Systemadministration 2 Übung
* SAM 2 Übung
* vLAB Storage


---
## Logical Volume Manager
* vLAB-Storage-LVM
* Version 20190327


---
## Agenda LVM
* Voraussetzungen vLAB
* Installation
* Einführung
* Umgebung vLAB
* First Steps
* Aufgaben
* Troubleshooting
* Cheatsheet
* Links
* Notizen

---
## LVM Voraussetzungen vLAB
* Hypervisor Virtualbox / HyperV
* 1 x CPU Core
* 1 GB RAM
* 3 x Disks mininum 1 GB
* Linux Ubuntu 18.04 LTS


---
## Vorbereitungen vLAB
* Vorbereitungen vLAB Vagrant
* Vorbereitungen vLAB Manual (optional)

---
## Vorbereitungen vLAB Vagrant
* git clone https://gitlab.com/rstumpner/sam-vlab-storage-lvm.git
* cd vlab/vagrant-virtualbox
* vagrant up
* vagrant ssh
* Username: vagrant
* Password: vagrant

---
## Vorbereitungen vLAB Manual 
#### optional
* VM Erstellen
* 1 CPU
* 2 GB RAM
* 2 GB Disk für Betriebssystem
* Hinzufügen Disks 3 x 1 GB
* Ubuntu 18.04 LTS Basisinstallation
  
---
## Logical Volume Manager
#### Ist eine Abstraktionsschicht zum Verwalten von Speicherzuweisungen unabhängig von den physikalischen Speicher Resourcen

---
### Logical Volume Manager

![LVM Aufbau](_images/lvm-aufbau.png)

---
## Physical Volume (PV)
#### Ist die Physikalische Speichereinheit eines Logical Volume Managers

---
## Volume Group (VG)
#### Ist der Logische Zusammenschluss der physikalischen Speichereinheiten eines Logical Volume Managers

---
## Logical Volume (LV)
#### Ist eine logische Einheit aus einer Volume Group

---
## vLAB Umgebung
![VM Konfiguration](_images/virtualbox-vm-struktur.png)

---
## First Steps in Logical Volume Manager
* Check der Hardware Speicher
* Erstellen einer Partition für LVM
* Erstellen eines Pysical Volumes
* Erstellen einer Volume Group
* Erstellen eines Logical Volumes

---
## Check der Hardware Speicher

```bash
lsblk
```

---
## Partitionierung der Harddisk
```bash
cfdisk /dev/sdx
```

---
## Partitionierung der Harddisk
#### Typen (8e)

![Partition Types](_images/partition-typen.png)

---
## Erstellen der Physical Volume
```md
pvcreate /dev/sdx
```
![pvcreate](_images/lvm-pvcreate.png)

---
## Sichtung des Physical Volume
```md
pvdisplay
```

---
## Erstellen der Volume Group

```md
vgcreate data /dev/sdx1
```

![pvcreate](_images/lvm-vgcreate.png)

---
## Sichtung der Volume Group
```md
vgdisplay
```

---
## Erstellen der Logical Volume
```md
lvcreate -L 20M -n backup data
```

---
## Sichtung der Logical Volume
```md
lvdisplay
```

![lvcreate](_images/lvm-lvcreate.png)

---
## Dateisystem Operationen
* Erstellen eines Dateisystems
```md
mkfs.ext4 /dev/mapper/backup
```

* Mounten des Dateisystems
```md
mount /dev/data/backup /mnt
```

---
## Aufgaben LVM Level 1
* Textdatei mit Inhalt auf der Partition des Logical Volume erstellen
* Physical Volume erweitern ( Disk 2 hinzufügen )
* Volume Group erweitern
* Logical Volume erweitern
* Dateisystem erweitern
* Überprüfen der Erweiterten Volume auf Datenintegrität

---
## LVM Spicker 1


---
## Aufgaben LVM Level 2
* Physical Volume erweitern ( Disk 3 hinzufügen )
* Migration der Daten auf die neue Disk (Disk3)
* Auspflegen der alten Disk (1) aus der Volume Group
* Lesen der Textdatei (ist noch der gleiche Inhalt vorhanden)
* LVM Übersicht (pvdisplay/vgdisplay/lvdisplay)

---
## LVM Spicker 2

---
## Aufgaben LVM Level 3
* Erstellen einer Textdatei
* Erstellen eines Snapshots
* Ändern des Inhalts
* Revert Snapshot

---
## LVM Erstellen einer Textdatei

echo "Malware Encrypted your Data" >  

---
## Troubleshooting
* Troubleshooting Vagrant and Hyper-V

---
## Troubleshooting Hyper-V
#### Um Vagrant mit VirtualBox und Installiertem Hyper-V zu betreiben ist es notwendig Hyper-V zu deaktivieren.

* Turn Hyper-V OFF
	* bcdedit /set hypervisorlaunchtype off

* Turn Hyper-V ON
	* bcdedit /set hypervisorlaunchtype auto

---
## LVM Cheat Sheet
* Physical Volume
	* pvs
	* pvdisplay
	* pvcreate
* Volume Group
	* vgs
	* vgdisplay
	* vgcreate
* Logical Volume
	* lvs
	* lvdisplay
	* lvcreate

---
## Links

* http://teknixx.com/40-useful-linux-commands/
* http://gparted.org/livecd.php

---
# Notizen
* Physical Volume (pvs / pvdisplay / pvcreate)
* Volume Group (vgdisplay / vgcreate)
* Logical Volume (lvdisplay / lvcreate)
